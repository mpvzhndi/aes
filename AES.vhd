---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------in the name of god------------------------------------------------------
-------------------------------------------------the final project of vhdl-------------------------------------------------
----------------------------------------  Autor	: Mohammad Taghi Pivezhandi------------------------------------------------	
------------------------Implementation of AES algoritm bAESd on vhdl programming language----------------------------------
--description: we have 8 process with different sensivity lists	that doing different parts of encoding the input matrix----
---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------
--------------------------main part of program port and gneric configuration and entity declaration------------------------
---------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------



   library IEEE;
	use IEEE.STD_LOGIC_1164.all;
	use work.PACKAGE_AES.all;

	entity AES is 
	generic (Nb : natural:=4;	--number of columns
	Nr: natural:=10);		   --number of rounds
	
	port (INPUT :in std_logic_vector(31 downto 0);	--input of system
	RESETN : in std_logic; --reset recognition	by 0
	COLUMN : in std_logic_vector(1 downto 0); -- stores in column that defines by this port 
	R_W : in std_logic;				--if R_W='1' read mode else if R_W='0' write mode
	SWITCH : in std_logic;--1 state  0 key 
	CLK	 : in std_logic;			  --system clk
	outp : out std_logic_vector(31 downto 0)); --output of system

	end AES;
---------------------------------------------------------------------------------------------------------------

	architecture behavioral of AES is 				   

	type matrix is array ((Nb-1) downto 0) of bit_vector(31 downto 0);
	type matrix_round is array ( 43 downto 0 ) of bit_vector(31 downto 0);

	shared variable round :matrix_round;--round key generator matrix the number of columns directly depends on number of rounds  
	--Shared variable is not of a protected type in (IEEE Std 1076-2002, 4.3.1.3)
	shared variable state :matrix;	  --state declaration that recieved input data
	shared variable key :matrix;	  --key for configuratin of round key 
	shared variable counter:integer:=0;	--counter for counting number of rounds
	shared variable state_add :matrix;	--storing changes on data that occors in the add round key transformation process 
	shared variable state_sub :matrix;  --storing changes on data that occors in the SubBytes transformation process
	shared variable state_shift :matrix; --storing changes on data that occors in the shift Rows transformation  process
	shared variable state_mix :matrix;	 --storing changes on data that occors in the mix columns transformation  process



	signal read_compleated:std_logic:='0';--this shows that reading process compleated
	signal round_compleated:std_logic:='0';--this shows that round key process compleated	
	
	signal addroundkey_enable:std_logic:='0';	--this shows that add round key enabled or not
	signal addroundkey_compleated:std_logic:='0';	--this shows that add round key compleated or not
	
	signal subbyte_enable:std_logic:='0';  --this shows that subbyte process enabled or not
	signal subbyte_compleated:std_logic:='0'; 	 --this shows that sub bytes process compleated or not
	
	signal shiftrows_enable:std_logic:='0';	 --this shows that Shift Row process compleated
	signal shiftrows_compleated:std_logic:='0';	   --this shows that shift Row process enabled or not
	
	signal mixcolumn_enable:std_logic:='0';	 --this shows that Mix Columns process enabled or not
	signal mixcolumn_compleated:std_logic:='0';	 --this shows that Mix Columns process compleated or not
	
	signal all_process_compleated:std_logic:='0';  --when all of processcompleated for output recognition set by 1
		
	signal write_compleated:std_logic:='1';--this shows that writting process compleated
	
	begin			 

---------------------------------------------------------------------------------------------------------
-----------------------------------------Reading Process-------------------------------------------------
---------------------------------------------------------------------------------------------------------


	read_process : process(INPUT,write_compleated,clk,SWITCH,R_W,COLUMN,RESETN) 
   begin	   	
	if RESETN='1' then
	if (R_W ='1') and (write_compleated='1') then
	read_compleated <= '0';			-- first we definning read to 0
   if clk'event and clk='1' then

 	case COLUMN is
		  
		when "00" =>
		if SWITCH ='1' then
		state(0) := to_bitvector(INPUT) ; --to_bitvector function is in package IEEE.STD_LOGIC_1164 to converting input to bit vector 
		else	 
		key(0) := to_bitvector(INPUT) ; 
		end if;
		when "01" =>
		if SWITCH ='1' then
		state(1) := to_bitvector(INPUT) ; 
		else	 
		key(1) := to_bitvector(INPUT) ; 
		end if;
		when "10" =>
		if SWITCH ='1' then
		state(2) := to_bitvector(INPUT) ; 
		else	 
		key(2) := to_bitvector(INPUT) ; 
		end if;
		when "11" =>
		if SWITCH ='1' then
		state(3) := to_bitvector(INPUT) ; 
		else	 
		key(3) := to_bitvector(INPUT) ; 
		end if;
		when others =>
		null;
		end case;
	else null;
	end if;
	else
	read_compleated <= '1';		-- after input violate (R_W ='1') and (write_compleated='1')  condition
	 end if;
	else
	for i in 0 to 3 loop
   state(i) := (others=>'0');
   key(i) := (others=>'0');
   end loop;
   end if;
end process;
------------------------------------------------main process------------------------------------------------
--------------------------this process is for connecting different process by enable or  disable them-------
------------------------------------------------------------------------------------------------------------


main_process: process(read_compleated,round_compleated,addroundkey_compleated,subbyte_compleated,shiftrows_compleated,mixcolumn_compleated)	

	begin	
	 if round_compleated ='1' and counter=0 and read_compleated ='1'  then --if this three condition get true the encryption starts
	addroundkey_enable <= '1'; 
	else 
	null;
	end if;

	if addroundkey_compleated='1' and counter=1 then 
		addroundkey_enable <= '0';
		    subbyte_enable <= '1';
		  else null;
   end if;	
																			 			
	if  counter /= 10  then 		  --loop for ten time
		
		if  subbyte_compleated = '1' then 
				 subbyte_enable <= '0';
			 shiftrows_enable <= '1';
	
		else null;
		 end if;
		 if  shiftrows_compleated='1' then 
			   shiftrows_enable <= '0';
				 mixcolumn_enable <= '1';
				
		 else null;
			 end if;
				 if  mixcolumn_compleated='1' then
				mixcolumn_enable <= '0';
	           addroundkey_enable <= '1';
			   else 
				  null;
			   end if;
			   if  addroundkey_compleated ='1' then
				addroundkey_enable <= '0';
				 subbyte_enable <= '1';
			   else null;
			   end if;
			   else null;
				   end if;
	
	if  counter = 10  then 			   -- final round recognition
		 if  addroundkey_compleated ='1' then
				addroundkey_enable <= '0';
				subbyte_enable <= '1';
		 else null;
			   end if;
		if  subbyte_compleated='1' then
			 subbyte_enable <= '0';
			 shiftrows_enable <= '1' ;
		else null;
			end if;
			 if  shiftrows_compleated ='1' then
	         	 shiftrows_enable <= '0';
				 addroundkey_enable <= '1';
			 else null;
				 end if;
				 if  addroundkey_compleated ='1' then
				 addroundkey_enable <= '0';
			else null;
			 end if;
			 else null;
			    end if;

	 end process;   
										  
-----------------------------------------------------------------------------------------------------	
---------------------------------------add round key process-----------------------------------------		
---------in this process state columns will xor with round key that created in round key process-----
-----------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------
	addroundkey_process: process(clk,addroundkey_enable,round_compleated)
    begin
	
	  if rising_edge(clk) then	 
	 if  addroundkey_enable='1' and  round_compleated='1' then		-- first round process most compleated
	
	 addroundkey_compleated <= '0';
	all_process_compleated <= '0';
	
	if counter=0 then	
	for i in 0 to (Nb-1) loop
	state_add(i) := state(i);		   --when we start
	end loop;
   elsif counter=10 then
	for i in 0 to (Nb-1) loop
	state_add(i) := state_shift(i);	   -- in middle round loop 
	end loop;
	
	else 
   for i in 0 to (Nb-1) loop
    state_add(i) := state_mix(i);		--	in final round
	end loop;
   end if;

	for i in 0 to (Nb-1) loop 
		state_add(i) := round (i+(4*counter)) xor state_add(i);	  --xor round key with state
		end loop; 
		counter := counter+1 ;	
	   	if counter=(Nr) then	
		counter:=0;
		all_process_compleated <= '1';
		else 
		all_process_compleated <= '0';				  
		end if;	 	
		addroundkey_compleated <= '1'; 
		else 
		addroundkey_compleated <= '0';
	end if;
else null;
end if;
end process;
-------------------------------------------------------------------------------------------
---------------------------------------subbyte process-------------------------------------
---------------------in this process we replace the code by sbox table code----------------	   
-------------------------------------------------------------------------------------------

	subbyte_process: process(clk,subbyte_enable) 
	variable statebuffer1 : bit_vector(7 downto 0);
	variable statebuffer2 : bit_vector(7 downto 0);
	begin	
	
	if clk'event and clk='1' then	
	if subbyte_enable='1' then
	
	subbyte_compleated <= '0';
	
	for i in 0 to (Nb-1) loop
	state_sub(i) := state_add(i);
	end loop;


	for j in 0 to (Nb-1) loop
	 for i in 0 to 3 loop
		statebuffer1 := state_sub(j)(((8*(i+1))-1) downto (8*i));
		sbox_procedure: sbox(statebuffer1,statebuffer2);--sbox look_up table procedure created in package AES
	 state_sub(j)(((8*(i+1))-1) downto (8*i)) := statebuffer2 ;
		 end loop;
	end loop;
	subbyte_compleated <= '1';
   
	else

    subbyte_compleated <= '0';
	
	end if;	
	else null;
		end if;
	end process;
	
------------------------------------------------------------------------------------------------------
----------------------------------------shift rows process--------------------------------------------
----------------------------this process create the shifted rows by rijndael algoritm-----------------
------------------------------------------------------------------------------------------------------

	shiftrows_process: process(clk,shiftrows_enable)
	variable shiftrowsconvert:bit_vector (((8*Nb)-1) downto 0 );	 --temporary for converting in shift row
	begin 
		if clk'event and clk='1' then	
		if shiftrows_enable = '1' then 
			shiftrows_compleated <= '0';
			for i in 0 to (Nb-1) loop
			state_shift := state_sub;
			end loop;
			
	
	for i in 0 to 3 loop
		for j in 0 to (Nb-1) loop
		shiftrowsconvert((8*(j+1)-1) downto (8*j)) := state_sub(j)((8*(i+1)-1) downto (8*i));
	end loop;
			
		shiftrowsconvert := shiftrowsconvert ror (8*i);
	
	    for j in 0 to (Nb-1) loop
		    state_shift(j)((8*(i+1)-1) downto (8*i)) := shiftrowsconvert((8*(j+1)-1) downto (8*j));
		end loop; 
				
	 		 end loop;	  
			shiftrows_compleated <= '1';
		else
			shiftrows_compleated <= '0';
		end if;	  
		else null;
			end if;
		end process;
		
-------------------------------------------------------------------------------------------------------------
------------------------------------------the mix column process--------------------------------------------- 
-------------we here use the finite field multiplication that declared in AES package -----------------------
-------------------------------------------------------------------------------------------------------------

	mixcolumn_process: process(clk,mixcolumn_enable)
	constant mixmatrix:bit_vector(31 downto 0):="00000001000000010000001100000010";	-- this array used for mixing state
	variable temp1:bit_vector(31 downto 0);										   --16#01010302#
	variable temp2:bit_vector(31 downto 0);
	begin 
	if clk'event  and clk='1' then	
		    				    
		if mixcolumn_enable = '1' then
		
		for i in 0 to (Nb-1) loop
		state_mix(i) := state_shift(i);
		end loop;
		
		mixcolumn_compleated <= '0';	 
	
		for i in 0 to Nb-1 loop  
			temp2 := state(i); 	
				for j in 0 to 3 loop
			temp1 := mixmatrix rol (8*j); 
		    state_mix(i)(((8*(j+1))-1) downto (8*j)) := FFM(temp2(7 downto 0),temp1(7 downto 0)) xor
			FFM(temp2(15 downto 8),temp1(15 downto 8)) xor FFM(temp2(23 downto 16),temp1(23 downto 16))
			xor FFM(temp2(31 downto 24),temp1(31 downto 24)); 	--finite field multiplication function
				end loop;
			end loop;	
			
		mixcolumn_compleated <= '1';
	
		else 	 
		mixcolumn_compleated<='0';
	end if;
	else null;
			end if;

	end process;												


------------------------------------------------------------------------------------------------------
--------------------------------------round key process-----------------------------------------------	
----this process is used for creating round keys with different procedures that we doing with key-----
------------------------------------------------------------------------------------------------------		
------------------------------------------------------------------------------------------------------		
			 
	roundkey_process : process(write_compleated,read_compleated,CLK)
	variable rcontemp: bit_vector(31 downto 0);
	variable roundtemp1: bit_vector(7 downto 0);  --this is for sbox procedure
	variable roundtemp2: bit_vector(31 downto 0); --this is for xor procedure
	variable count5 :integer:=0; 
	variable count6 :integer:=0;
	begin
 if write_compleated='1' and read_compleated ='1' then
 
    round_compleated <= '0';
	
	for i in 0 to (Nb-1) loop 	 
			round(i) := key(i); --this is for round configuration proocess
		end loop;
 if count5 /= (Nr-1)  then--counter for 128 bit is Nr=11
if clk'event and clk = '1' then
	roundtemp2 := round(((Nb-1)*(count5 + 1)) + count5) ror 8 ;--rotational shift right of 4th round column
		for i in 0 to 3 loop
			for j in 0 to 7 loop
				roundtemp1(j) := roundtemp2(8*i + j);
			end loop;									 
	sbox_procedure : sbox(roundtemp1,roundtemp2(((7*(i+1))+i) downto ((7*i) + i))); 	--we here use sbox to convert
		end loop; 											-- roundtemp1 and store it in roundtemp2		  
		
		rconprocedure: rcon(count5,rcontemp); --rcon look up table defined in AES package
      roundtemp2 := rcontemp xor roundtemp2;
		
		for i in 0 to 3 loop
		round (i + (4 * (count5 + 1))) := round(i + (4*count5)) xor roundtemp2;
		roundtemp2 := round (i + (4 * (count5 + 1)));
		end loop;															   
	   count5 := count5 + 1; 
  	else 
	null;
		end if;
else 
round_compleated <= '1';
end if;
	    else
		  null;
   	end if;
	
   end process;

   
-----------------------------------------------------------------------------------------	
---------------------------------write process-------------------------------------------
------------------------state and key will be going out from pins here-------------------	
-----------------------------------------------------------------------------------------

	


   write_process: process(clk,SWITCH,R_W,column,all_process_compleated)	
 
  	begin	
	
	if all_process_compleated='1' then
	write_compleated <= '0';
	if R_W='0' and clk'event and clk='1' then
		
		case COLUMN is
	when "00" =>
    if SWITCH ='1' then
	outp <= to_stdlogicvector(state(0)) ; 
	else	 
		outp <= to_stdlogicvector(key(0)) ; 
	end if;
	when "01" =>
	if SWITCH ='1' then
		outp <= to_stdlogicvector(state(1)) ; 
	else	 
		outp <= to_stdlogicvector(key(1)) ; 
	end if;
	when "10" =>
	if SWITCH ='1' then
	outp <= to_stdlogicvector(state(2)) ; 
	else	 
		outp <= to_stdlogicvector(key(2)) ; 
	end if;
	when "11" =>
	if SWITCH ='1' then
	outp <= to_stdlogicvector(state(3)) ; 
	else	 
		outp <= to_stdlogicvector(key(3)) ; 
		end if;
	 when others =>
	 null;
	end case;
		
	else null;
	end if;
		else
	write_compleated <= '1';
	outp <= (others => 'Z');
	end if;

	end process;  
	
	end behavioral;
------------------------------------------------------------------------------------
------------------------------------THE END---------------------------------------
------------------------------------------------------------------------------------