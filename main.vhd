	library IEEE;
	use IEEE.STD_LOGIC_1164.all;
	use work.PACKAGE_AES.all;
	use IEEE.std_logic_arith.all;
    use IEEE.std_logic_unsigned.all;

	entity main is 
	generic (Nb : natural:=4;
	Nr: natural:=11);
	
	port (INPUT :in std_logic_vector(31 downto 0);
	RESETN : in std_logic;
	COLUMN : in std_logic_vector(1 downto 0);
	R_W : in std_logic;
	SWITCH : in std_logic;--1 state  0 key 
	CLK	: in std_logic;
	outp : out std_logic_vector(127 downto 0)) ;
	
	end main;

	architecture main of main is 				   

	type matrix is array ((Nb-1) downto 0) of bit_vector(31 downto 0);
	type matrix_round is array ( 43 downto 0 ) of bit_vector(31 downto 0);

	shared variable round :matrix_round;
	shared variable state :matrix;
	shared variable state_add :matrix;
	shared variable state_sub :matrix;
	shared variable state_shift :matrix;
	shared variable state_mix :matrix;

	shared variable key :matrix;
	shared variable counter:integer:=0;

	signal read_compleated:std_logic:='0';--this shows that reading process compleated
	signal write_compleated:std_logic:='1';--this shows that writting process compleated
	signal round_compleated:std_logic:='0';--this shows that round key compleatedley created	
	signal subbyte_enable:std_logic:='0';
	signal shiftrows_enable:std_logic:='0';
	signal mixcolumn_enable:std_logic:='0';
	signal addroundkey_enable:std_logic:='0';
	signal subbyte_compleated:std_logic:='0'; 
	signal shiftrows_compleated:std_logic:='0';
	signal mixcolumn_compleated:std_logic:='0';
	signal addroundkey_compleated:std_logic:='0';
	signal all_process_compleated:std_logic:='0';
	
	begin			 

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------


		read_process : process(INPUT,write_compleated,clk,SWITCH,R_W,COLUMN,RESETN) 

	begin	   	

	if RESETN='1' then
	
	if (R_W ='1') and (write_compleated='1') then
	
	read_compleated <= '0';	

	if clk'event and clk='1' then

 	 case COLUMN is
		when "00" =>
		if SWITCH ='1' then
		state(0) := to_bitvector(INPUT) ; 
		else	 
		key(0) := to_bitvector(INPUT) ; 
		end if;
		when "01" =>
		if SWITCH ='1' then
		state(1) := to_bitvector(INPUT) ; 
		else	 
		key(1) := to_bitvector(INPUT) ; 
		end if;
		when "10" =>
		if SWITCH ='1' then
		state(2) := to_bitvector(INPUT) ; 
		else	 
		key(2) := to_bitvector(INPUT) ; 
		end if;
		when "11" =>
		if SWITCH ='1' then
		state(3) := to_bitvector(INPUT) ; 
		else	 
		key(3) := to_bitvector(INPUT) ; 
		end if;
		when others =>
		null;
		end case;
		
	else null;
	end if;
	
	else
	
	read_compleated <= '1';
	
   end if;
	
	else
	
	for i in 0 to 3 loop
   state(i) := (others=>'0');
   key(i) := (others=>'0');
   end loop;

   end if;
end process;
  ----------------------------------------------------------------------------------------------------------
 main_process: process(write_compleated,read_compleated,round_compleated,addroundkey_compleated,subbyte_compleated,shiftrows_compleated,mixcolumn_compleated)	

 begin	
	 if write_compleated='1' then
	 if round_compleated ='1' and counter=0 and read_compleated ='1'  then  
	addroundkey_enable <= '1'; 	 
	end if;
	if addroundkey_compleated='1' and counter=1 then 
		addroundkey_enable <= '0';
		    subbyte_enable <= '1';
	end if;	
																			 			
	if  counter /= 10  then 	

		if  subbyte_compleated = '1' then 
				 subbyte_enable <= '0';
			 shiftrows_enable <= '1';
		end if;
		
		 if  shiftrows_compleated='1' then 
			   shiftrows_enable <= '0';
				 mixcolumn_enable <= '1';	   
			 end if;
				 if  mixcolumn_compleated='1' then
				mixcolumn_enable <= '0';
	           addroundkey_enable <= '1'; 
			   end if;
			   if  addroundkey_compleated ='1' then
				addroundkey_enable <= '0';
				 subbyte_enable <= '1';
			   end if;				   
				   end if;
	
	if  counter = 10  then 	
		 if  addroundkey_compleated ='1' then
				addroundkey_enable <= '0';
				subbyte_enable <= '1';
			   end if;
		if  subbyte_compleated='1' then
			 subbyte_enable <= '0';
			 shiftrows_enable <= '1' ;	

			end if;
			 if  shiftrows_compleated ='1' then
	         	 shiftrows_enable <= '0';
				 addroundkey_enable <= '1';	  
				 end if;
				 if  addroundkey_compleated ='1' then
				 addroundkey_enable <= '0';	  
			 end if;					  
			    end if;					 
				  end if;

	 end process;   
										  
	
		
------------------------------------------------------------------------------------------------------
	addroundkey_process: process(clk,addroundkey_enable,round_compleated)
    begin
	 
	if  addroundkey_enable='1' and round_compleated='1' then
	addroundkey_compleated <= '0';
	if rising_edge(clk) then 
	all_process_compleated <= '0';
	
	if counter=0 then	
	for i in 0 to (Nb-1) loop
	state_add(i) := state(i);
	end loop;
    elsif counter=10 then
	for i in 0 to (Nb-1) loop
	state_add(i) := state_shift(i);
	end loop;
	
	else 

	for i in 0 to (Nb-1) loop
    state_add(i) := state_mix(i);
	end loop;

    end if;

	for i in 0 to (Nb-1) loop 
		state_add(i) := round (i+(4*counter)) xor state_add(i);
		end loop; 
		
	   counter := counter+1 ;	
		if counter=Nr then	
		counter:=0;
		all_process_compleated <= '1';
		else 
		all_process_compleated <= '0';				  
		end if;	 	
		addroundkey_compleated <= '1'; 
		else null;
		end if;
		
		else 
			addroundkey_compleated <= '0';
	end if;

end process;
-------------------------------------------------------------------------------------------
---------------------------------------subbyte process-------------------------------------
-------------------------------------------------------------------------------------------

	subbyte_process: process(clk,subbyte_enable) 
	variable statebuffer1 : bit_vector(7 downto 0);
	variable statebuffer2 : bit_vector(7 downto 0);
	begin	
	
	if clk'event and clk='1' then	
	if subbyte_enable='1' then
	
	subbyte_compleated <= '0';
	
	for i in 0 to (Nb-1) loop
	state_sub(i) := state_add(i);
	end loop;


	for j in 0 to (Nb-1) loop
				for i in 0 to 3 loop
					statebuffer1 := state_sub(j)(((8*(i+1))-1) downto (8*i));
			   sbox_procedure: sbox(statebuffer1,statebuffer2);
			  state_sub(j)(((8*(i+1))-1) downto (8*i)) := statebuffer2 ;
			   end loop;
			end loop;
			subbyte_compleated <= '1';

	else

   subbyte_compleated <= '0';
	
	end if;	
	else null;
		end if;
	end process;
	
------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------

	shiftrows_process: process(clk,shiftrows_enable)
	variable shiftrowsconvert:bit_vector (((8*Nb)-1) downto 0 );
	begin 
		if clk'event and clk='1' then	
		if shiftrows_enable = '1' then 
			shiftrows_compleated <= '0';
			for i in 0 to (Nb-1) loop
			state_shift := state_sub;
			end loop;
			
			
			for i in 0 to 3 loop
				for j in 0 to (Nb-1) loop
				shiftrowsconvert((8*(j+1)-1) downto (8*j)) := state_sub(j)((8*(i+1)-1) downto (8*i));
				end loop;
			
			shiftrowsconvert := shiftrowsconvert ror (8*i);
			
			    for j in 0 to (Nb-1) loop
				    state_shift(j)((8*(i+1)-1) downto (8*i)) := shiftrowsconvert((8*(j+1)-1) downto (8*j));
				end loop; 
				
	 		 end loop;	  
			 shiftrows_compleated <= '1';
		else
			shiftrows_compleated <= '0';
		end if;	  
		else null;
			end if;
		end process;
		
-------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------

	mixcolumn_process: process(clk,mixcolumn_enable)
	constant mixmatrix:bit_vector(31 downto 0):="00000001000000010000001100000010";
	variable temp1:bit_vector(31 downto 0);
	variable temp2:bit_vector(31 downto 0);
	begin 
	if clk'event  and clk='1' then	
		    				    
		if mixcolumn_enable = '1' then
		
		for i in 0 to (Nb-1) loop
		state_mix(i) := state_shift(i);
		end loop;
		
		mixcolumn_compleated <= '0';	 
	
		for i in 0 to Nb-1 loop  
			temp2 := state_mix(i); 	
				for j in 0 to 3 loop
			temp1 := mixmatrix rol (8*j); 
		    state_mix(i)(((8*(j+1))-1) downto (8*j)) := FFM(temp2(7 downto 0),temp1(7 downto 0)) xor FFM(temp2(15 downto 8),temp1(15 downto 8)) 
			xor FFM(temp2(23 downto 16),temp1(23 downto 16)) xor FFM(temp2(31 downto 24),temp1(31 downto 24)); 
				end loop;
			end loop;	
			
				    mixcolumn_compleated <= '1';
	
		else 	 
		mixcolumn_compleated<='0';
	end if;
	else null;
			end if;

	end process;												



------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------		
------------------------------------------------------------------------------------------------------		
			 
	roundkey_process : process(write_compleated,read_compleated,CLK)
	variable rcontemp: bit_vector(31 downto 0);
	variable roundtemp1: bit_vector(7 downto 0);  --this is for sbox procedure
	variable roundtemp2: bit_vector(31 downto 0); --this is for xor procedure
	variable count5 :integer:=0; 
	variable count6 :integer:=0;
	
	begin
	 if write_compleated='1' then
	if read_compleated ='1' then
    round_compleated <= '0';
	
	for i in 0 to (Nb-1) loop 	 
			round(i) := key(i); --this is for round configuration proocess
		end loop;
 if count5 /= (Nr-1)  then

 
  if clk'event and clk = '1' then
			
		roundtemp2 := round(((Nb-1)*(count5 + 1)) + count5) ror 8 ;--rotational shift right of 4th round column
		
		for i in 0 to 3 loop
			for j in 0 to 7 loop
				roundtemp1(j) := roundtemp2(8*i + j);
			end loop;									 
		sbox(roundtemp1,roundtemp2(((7*(i+1))+i) downto ((7*i) + i))); 
		end loop; 													  
		
		rconprocedure: rcon(count5,rcontemp);
      roundtemp2 := rcontemp xor roundtemp2;
		
		for i in 0 to 3 loop
		
		
		round (i + (4 * (count5 + 1))) := round(i + (4*count5)) xor roundtemp2;
		roundtemp2 := round (i + (4 * (count5 + 1)));
		
		end loop;															   

		count5 := count5 + 1; 

	else 
	null;
		end if;
else 
round_compleated <= '1';
end if;
				  
   	end if;		 
	  end if;
   end process;



-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------	
	


   write_process: process(clk,SWITCH,R_W,all_process_compleated)	
 
  	begin	
	
	if all_process_compleated='1' then
	write_compleated <= '0';
	
	if R_W='0'and clk'event and clk='1' then
		
    if SWITCH ='1' then
		for i in 0 to (Nb-1) loop
		outp(((32*(i+1))-1) downto (32*i)) <= to_stdlogicvector(state_add(i));
		end loop;
	else
		for i in 0 to (Nb-1) loop
		outp(((32*(i+1))-1) downto (32*i)) <= to_stdlogicvector(key(i)) ;
		end loop;
		
	end if;	  
				 
	end if;
		else
	write_compleated <= '1';
	outp <= (others => 'Z');
	end if;

	end process;  
	
	end main;
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------