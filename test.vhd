
	library IEEE;
	use IEEE.STD_LOGIC_1164.all;
	use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity test is 
	generic (Nb : natural:=4;
		Nr: natural:=11);
	
	end test;
	
	--}} End of automatically maintained section
	
	architecture test of test is
	signal INPUT : std_logic_vector(31 downto 0);
	signal RESETN : std_logic;
	signal R_W : std_logic;	
	signal SWITCH : std_logic; -- "1" state  "0" key 
	signal COLUMN : std_logic_vector(1 downto 0);--	"10" state   others null
	signal CLK	: std_logic;
	signal OUTP : std_logic_vector(127 downto 0);
	
	begin 	
		process
		begin
			clk <= '1';
			wait for 1 ns;
			clk <= '0'; 
			wait for 1 ns;
		end process;
		R_W <= '1' ,'0' after 40ns;	 
		RESETN <= '0','1' after 1 ns; 
		switch <= '1', '0' after 20 ns,
		'1' after 50ns;
		lut: entity work.main generic map(Nb=>Nb,Nr=>Nr) port map(INPUT,RESETN,COLUMN,R_W,SWITCH,CLK,OUTP);
			
			process
			begin	 
	    
		COLUMN <= "00";	
		input <= "10101000111101100100001100110010";--a8f64332
		wait for 5ns;
		COLUMN <= "01";   
		input <= "10001101001100000101101010001000";--8d305a88
		wait for 5ns;
		COLUMN <= "10";								
		input <= "10100010100110000011000100110001";--a2983131
		wait for 5ns;
		COLUMN <= "11";								
		input <= "00110100000001110011011111100000";--340737e0
		wait for 5ns ; 
		
		
		COLUMN <= "00";
		
		input <= "00010110000101010111111000101011";--16157e2b
		wait for 5ns;
		COLUMN <= "01";   
		input <= "10100110110100101010111000101000";--a6d2ae28
		wait for 5ns;
		COLUMN <= "10";								
		input <= "10001000000101011111011110101011";--8815f7ab
		wait for 5ns;
		COLUMN <= "11";								
		input <= "00111100010011111100111100001001";--3c4fcf09
		wait for 5ns ;
			end process; 
		
		
		
		
	end test;
